﻿using System.Windows.Media.Imaging;

namespace _27._1
{
    class CommissieWerker : Werknemer
    {


        private int _aantal;
        private double _commissie;

        public int Aantal { get { return _aantal; } set { if (value > 0) { _aantal = value; } } }

        public double Commissie { get { return _commissie; } set { if (value > 0) { _commissie = value; } } }

        public CommissieWerker(string naam, string voornaam, double loon, double commissie, int aantal, BitmapImage geslacht) : base(naam, voornaam, loon, geslacht)
        {
            Commissie = commissie;
            Aantal = aantal;
        }


        public override double Verdiensten()
        {
            return Loon + Aantal * Commissie;
        }

        //Zelfde fout, zie Uurwerker!
        public override string ToString()
        {
            return base.ToString() + " " + "CommissieWerker";
            //return " " + Voornaam + " " + Naam + "€" + Verdiensten();
        }



    }
}
