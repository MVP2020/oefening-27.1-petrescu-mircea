﻿using System;
using System.Windows.Media.Imaging;

namespace _27._1
{
    class UurWerker : Werknemer
    {
        private double _uren;

        public double Uren { get { return _uren; } set { if (value > 0) { _uren = value; } } }

        public UurWerker(string naam, string voornaam, double loon, int aantalUren, BitmapImage geslacht) : base(naam, voornaam, loon, geslacht)
        {
            Uren = aantalUren;
            Geslacht = geslacht;
        }


        public override double Verdiensten()
        {
            double eenLoon = Loon * Convert.ToDouble(Uren);

            if (Uren > 40)
            {
                eenLoon += Convert.ToDouble(Uren - 40) * (Loon * 1.5);
            }
            return eenLoon;
        }

        public override string ToString()
        {
            return base.ToString() + " " + "Uurwerker";
        }

    }
}
