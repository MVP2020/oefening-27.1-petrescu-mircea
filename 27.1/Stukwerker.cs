﻿using System.Windows.Media.Imaging;

namespace _27._1
{
    class StukWerker : Werknemer
    {
        private int _aantal;

        public int Aantal { get { return _aantal; } set { if (value > 0) { _aantal = value; } } }
        public StukWerker(string naam, string voornaam, double loonPerStuk, int aantal, BitmapImage geslacht) : base(naam, voornaam, loonPerStuk, geslacht)
        {
            Aantal = aantal;
        }




        public override double Verdiensten()
        {
            return Loon * Aantal;
        }


        //ToString fouten bij overerving! Let daar op. =-)
        public override string ToString()
        {
            return base.ToString() + " " + "Stukwerker";
            //Fout:
            // return " " + Voornaam + " " + Naam + "€" + Verdiensten();
        }

    }
}
