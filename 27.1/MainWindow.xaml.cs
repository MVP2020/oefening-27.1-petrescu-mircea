﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace _27._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {



        public MainWindow()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {

            if (rdMan.IsChecked == true)
            {
                BitmapImage geslacht = new BitmapImage(new Uri(@"Images\mannelijk_teken.png", UriKind.Relative));



                if (rdCommissie.IsChecked == true)
                {

                    CommissieWerker commisieWerker = new CommissieWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), Convert.ToDouble(txtLoon.Text), Convert.ToDouble(txtCommissie.Text), Convert.ToInt32(txtAantal.Text), geslacht);
                    lbWerknemers.Items.Add(commisieWerker);
                }
                else if (rdStuk.IsChecked == true)
                {

                    StukWerker stukWerker = new StukWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loonPerStuk: (Convert.ToDouble(txtLoon.Text)), aantal: Convert.ToInt32(txtAantal.Text), geslacht);
                    lbWerknemers.Items.Add(stukWerker);
                }
                else if (rdUur.IsChecked == true)
                {
                    UurWerker uurWerker = new UurWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loon: (Convert.ToDouble(txtLoon.Text)), aantalUren: (Convert.ToInt32(txtAantal.Text)), geslacht);
                    lbWerknemers.Items.Add(uurWerker);
                }
                else if (rdWerknemer.IsChecked == true)
                {
                    Werknemer werknemer = new Werknemer(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loon: (Convert.ToDouble(txtLoon.Text)), geslacht);
                    lbWerknemers.Items.Add(werknemer);
                }
                else
                {
                    MessageBox.Show("Please select a worker type!");
                }
            }
            else if (rdVrouw.IsChecked == true)
            {

                BitmapImage imgvrouw = new BitmapImage(new Uri(@"Images\vrouwelijk_teken.jpg", UriKind.Relative));



                if (rdCommissie.IsChecked == true)
                {
                    CommissieWerker commisieWerker = new CommissieWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), Convert.ToDouble(txtLoon.Text), Convert.ToDouble(txtCommissie.Text), Convert.ToInt32(txtAantal.Text), geslacht: imgvrouw);
                    lbWerknemers.Items.Add(commisieWerker);
                }
                else if (rdStuk.IsChecked == true)
                {
                    StukWerker stukWerker = new StukWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loonPerStuk: (Convert.ToDouble(txtLoon.Text)), aantal: Convert.ToInt32(txtAantal.Text), geslacht: imgvrouw);
                    lbWerknemers.Items.Add(stukWerker);
                }
                else if (rdUur.IsChecked == true)
                {
                    UurWerker uurWerker = new UurWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loon: (Convert.ToDouble(txtLoon.Text)), aantalUren: (Convert.ToInt32(txtAantal.Text)), geslacht: imgvrouw);
                    lbWerknemers.Items.Add(uurWerker);
                }
                else if (rdWerknemer.IsChecked == true)
                {
                    Werknemer werknemer = new Werknemer(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loon: (Convert.ToDouble(txtLoon.Text)), geslacht: imgvrouw);
                    lbWerknemers.Items.Add(werknemer);
                }
                else
                {
                    MessageBox.Show("Please select a worker type!");
                }
            }
        }

        private void rdWerknemer_Checked(object sender, RoutedEventArgs e)
        {
            InstellenControls(Visibility.Hidden, "", "", Visibility.Hidden, "", "", rdWerknemer);
        }

        private void rdUur_Checked(object sender, RoutedEventArgs e)
        {
            InstellenControls(Visibility.Visible, "", "Aantal Uren: ", Visibility.Hidden, "", "", rdUur);
        }

        private void rdStuk_Checked(object sender, RoutedEventArgs e)
        {
            InstellenControls(Visibility.Visible, "", "Aantal: ", Visibility.Hidden, "", "", rdStuk);
        }

        private void rdCommissie_Checked(object sender, RoutedEventArgs e)
        {
            InstellenControls(Visibility.Visible, "", "Aantal: ", Visibility.Visible, "", "Commissie: ", rdCommissie);
        }
        private void InstellenControls(Visibility txtExtravisibility, string txtExtraText, string lblExtraContent, Visibility txtComissieVisibility, string txtComissieText, string lblComissieContent, RadioButton r)
        {
            txtAantal.Visibility = txtExtravisibility;
            txtAantal.Text = txtExtraText;
            lblAantal.Content = lblExtraContent;

            txtCommissie.Visibility = txtComissieVisibility;
            txtCommissie.Text = txtComissieText;
            lblCommissie.Content = lblComissieContent;

            grpWerker.Tag = r;
        }

        private void rdMan_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rdVrouw_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}

