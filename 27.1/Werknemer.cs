﻿using System.Windows.Media.Imaging;

namespace _27._1
{
    class Werknemer
    {
        private double _loon;
        private string _naam;
        private string _voornaam;
        private BitmapImage _geslacht;
        public BitmapImage Geslacht { get; set; }
        public double Loon { get { return _loon; } set { if (value > 0) { _loon = value; } } }
        public string Naam { get; set; }

        public string Voornaam { get; set; }


        public Werknemer(string naam, string voornaam, double loon, BitmapImage geslacht)
        {
            Naam = naam;
            Voornaam = voornaam;
            Loon = loon;
            Geslacht = geslacht;

        }

        public virtual double Verdiensten()
        {
            return Loon;
        }

        //public override bool Equals(obejct obj)
        // {

        // }
        public override string ToString()
        {
            return " " + Voornaam + " " + Naam + " € " + Verdiensten().ToString();
        }

        public string Gegevens => ToString();

        //Equals???? Vergeet die niet. 
        public override bool Equals(object obj)
        {
            //Copy paste this:
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            //Do not copy paste this. 1. Explicit cast. 2. Wright your own condition. 
            // Explicit cast:
            Werknemer w = (Werknemer)obj;
            //Your own condition:
            return this.Naam.Equals(w.Naam) && this.Voornaam.Equals(w.Voornaam);
        }
    }
}
